//
// 2DKit - Rapid game development
// http://2dkit.com

package yourproject;

import kit.Component;
import ez.EzApp;

import ecs.objects2dkit.ECSSetup;

/** The main component class. */
class YourProject extends Component
{
    /** Called when the component is started. */
    
    override public function onStart ()
    {
        owner.add(new ECSSetup());
        owner.add(new EzApp("Home"));
    }
}
