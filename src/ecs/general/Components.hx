package ecs.general;

import edge.IComponent;

class CptHealth implements IComponent
{
    var value : Float;
}

class CptDamage implements IComponent
{
    var value : Float;
}

class CptName implements IComponent
{
    var label : String;
}

class CptSpriteSrc implements IComponent
{
    var url : String;
}

class CptSoundSrc implements IComponent
{
    var url : String;
}

class CptPosition implements IComponent
{
    var x : Float;
    var y : Float;
}