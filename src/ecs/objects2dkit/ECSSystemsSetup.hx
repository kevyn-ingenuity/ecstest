package ecs.objects2dkit;

import edge.*;

import ecs.render2dkit.*;
import ecs.id.*;

class ECSSystemsSetup
{
    public static function AddSystems(world : World)
    {
        //add features here
        IDSystems.Setup(world);
        RenderSystems.Setup(world);
    }
}