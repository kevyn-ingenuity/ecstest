package ecs.objects2dkit;

import kit.creator.CreatorObject;
import kit.System;

import edge.*;

import ecs.objects2dkit.ECSSetup;
import ecs.id.Components;


class ECSEntity extends CreatorObject
{
    public var myComponents : String;

    public var entity(default, null) : edge.Entity;

    public function new ()
    {
        var engine : edge.Engine = System.root.get(ECSSetup).world.engine;
        this.entity = engine.create([new CptID(-1)]);
    }

    override public function onStart()
    {
        if (myComponents != null)
        {
            owner.emitMessageToParents(myComponents, owner);
        }
    }
}