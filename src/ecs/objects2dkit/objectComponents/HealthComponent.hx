package ecs.objects2dkit.objectComponents;

import kit.creator.CreatorAction;
import kit.Entity;
import kit.System;

import edge.*;

import ecs.objects2dkit.ECSEntity;
import ecs.general.Components;

class HealthComponent extends CreatorAction
{
    //place your component fields here for 2dkit inspector
    public var points : Float;

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).entity;
        entity.add(new CptHealth(points));
    }
}