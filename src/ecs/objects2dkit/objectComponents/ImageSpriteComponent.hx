package ecs.objects2dkit.objectComponents;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;
import kit.creator.SceneInfo;

import edge.*;

import ecs.objects2dkit.ECSEntity;
import ecs.render2dkit.Components;
import ecs.general.Components;

class ImageSpriteComponent extends CreatorAction
{
    public var imageName : String;

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).entity;
        //entity.add(new CptSpriteSrc(imageUrl));
        entity.add(new CptSpriteSrc(imageName));
    }
}