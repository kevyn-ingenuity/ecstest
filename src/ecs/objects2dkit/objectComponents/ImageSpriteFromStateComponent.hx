package ecs.objects2dkit.objectComponents;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;
import kit.creator.SceneInfo;

import edge.*;

import ecs.objects2dkit.ECSEntity;
import ecs.render2dkit.Components;
import ecs.general.Components;

class ImageSpriteFromStateComponent extends CreatorAction
{
    public var imageStateName : String;

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).entity;
        //entity.add(new CptSpriteSrc(imageUrl));
        var myStates : Map<String, String> = target.get(CreatorObject).info.states;
        entity.add(new CptSpriteSrc(myStates.get(imageStateName)));
    }
}