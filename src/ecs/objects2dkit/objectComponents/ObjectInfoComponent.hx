package ecs.objects2dkit.objectComponents;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;
import kit.creator.SceneInfo;

import edge.*;

import ecs.objects2dkit.ECSEntity;
import ecs.render2dkit.Components;

class ObjectInfoComponent extends CreatorAction
{

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).entity;
        entity.add(new CptObjectInfo(target.get(CreatorObject).info));
    }
}