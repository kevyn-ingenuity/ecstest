package ecs.objects2dkit.objectComponents;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;
import kit.creator.SceneInfo;

import edge.*;

import ecs.objects2dkit.ECSEntity;
import ecs.render2dkit.Components;
import ecs.general.Components;

class KitEntityComponent extends CreatorAction
{
    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).entity;
        //entity.add(new CptSpriteSrc(imageUrl));
        entity.add(new CptKitView(target));
    }
}