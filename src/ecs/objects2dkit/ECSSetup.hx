package ecs.objects2dkit;

import kit.Component;
import kit.System;

import edge.*;

import ecs.objects2dkit.ECSSystemsSetup;

class ECSSetup extends Component
{
    public var world(default, null) : World;

    override public function onStart()
    {
        //setup ecs world
        world = new World();
        ECSSystemsSetup.AddSystems(world);
        world.start();
    }
}