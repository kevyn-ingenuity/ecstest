package ecs.render2dkit;

import ecs.render2dkit.*;
import ecs.render2dkit.Components;

import kit.System;
import kit.scene.Director;

import edge.*;

class RenderSystems
{
    public static function Setup (world : World)
    {
        world.render.add(new LoadAssetsSystem());
        world.render.add(new AddImageSpriteSystem());
        world.render.add(new FactorySceneSystem());
        world.render.add(new SetActiveSceneSystem());
        world.render.add(new AddToSceneSystem());
    }
}