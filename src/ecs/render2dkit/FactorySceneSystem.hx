package ecs.render2dkit;

import edge.*;

import ecs.render2dkit.Components;
import ecs.general.Components;

import kit.Entity;
import kit.Component;
import kit.scene.Scene;

class FactorySceneSystem implements ISystem
{

    public function updateAdded(entity : edge.Entity, data : { newScene : CptCreateScene })
    {
        var newScene = new Entity();
        newScene.add(new Scene());
        
        entity.addMany([
            new CptKitView(newScene),
            new CptName(data.newScene.name),
            new CptScene()
        ]);

        entity.removeType(CptCreateScene);
    }
    
    public function update (newScene : CptCreateScene) {}
}