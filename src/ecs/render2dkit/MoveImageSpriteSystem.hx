package ecs.render2dkit;

import edge.*;

import ecs.render2dkit.Components;
import ecs.general.Components;

import kit.display.ImageSprite;

class MoveImageSpriteSystem
{
    var entity : Entity;

    public function update (imageSprite : CptImageSprite, position : CptPosition)
    {
        imageSprite.instance.setXY(position.x, position.y);
    }
}