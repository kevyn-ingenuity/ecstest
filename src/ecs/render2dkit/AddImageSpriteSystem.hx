package ecs.render2dkit;

import edge.*;

import ecs.render2dkit.Components;
import ecs.general.Components;

import kit.display.Texture;
import kit.display.ImageSprite;

class AddImageSpriteSystem implements ISystem
{
    var assetsTargets : edge.View<{ assetsCache : CptAssetsPackCache }>;

    var entity : Entity;

    public function update(sprite : CptSpriteSrc, view : CptKitView)
    {
        for (assets in assetsTargets)
        {
            var cache = assets.data.assetsCache.assets;
            try
            {
                var normal = cache.getTexture(entity.get(CptSpriteSrc).url, false);
                if (normal != null)
                {
                    var imageSprite = new ImageSprite(normal);
                    imageSprite.texture = normal;
                    imageSprite.centerAnchor();

                    //add to ecs entity
                    entity.add(new CptImageSprite(imageSprite));

                    //add to 2dkit entity
                    view.instance.add(imageSprite);

                    //remove to not load again
                    entity.removeType(CptSpriteSrc);
                }
            }
            catch(msg : String)
            {
                trace("Might be a Missing texture, check sprite url!");
            }
            
        }
    }
}