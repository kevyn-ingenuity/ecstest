package ecs.render2dkit;

import ecs.render2dkit.Components;
import ecs.general.Components;

import kit.scene.Scene;

import edge.*;

class AddToSceneSystem implements ISystem
{

    var scenes : edge.View<{ scene : CptScene, view : CptKitView, name : CptName }>;

    var entity : Entity;

    public function update (addToScene : CptAddToScene, view : CptKitView)
    {
        for (scene in scenes)
        {
            if (scene.data.name.label == addToScene.name)
            {
                scene.data.view.instance.addChild(view.instance);
                entity.removeType(CptAddToScene);
            }
        }
    }
}