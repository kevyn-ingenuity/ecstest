package ecs.render2dkit;

import edge.*;
import kit.Entity;
import kit.Assets;
import kit.creator.SceneInfo;
import kit.display.ImageSprite;
import kit.asset.AssetPack;
import kit.util.Promise;
import kit.scene.Transition;

class CptKitView implements IComponent
{
    var instance : kit.Entity;
}

class CptAddToScene implements IComponent
{
    var name : String;
}

class CptCreateScene implements IComponent 
{
    var name : String;
}

class CptSetActiveScene implements IComponent
{
    var name :String;
    var transition : Transition;
}

class CptScene implements IComponent {}

class CptDirector implements IComponent {}

class CptAssetsPackCache implements IComponent
{
    var assets : AssetPack;
}

class CptAssetsSrc implements IComponent
{
    var url : String;
}

class CptAssetsLoading implements IComponent
{
    var promise : Promise<Dynamic>;
}

class CptImageSprite implements IComponent
{
    var instance : ImageSprite;
}

class CptObjectInfo implements IComponent
{
    var info : ObjectInfo;
}

class CptSceneInfo implements IComponent
{
    var info : SceneInfo;
}