package ecs.render2dkit;

import edge.*;

import ecs.render2dkit.Components;

import kit.Assets;
import kit.asset.AssetPack;

class LoadAssetsSystem implements ISystem
{
    public function updateAdded(entity : Entity, data : { CptAssetsSrc : CptAssetsSrc })
    {
        var load = new Assets().load(data.CptAssetsSrc.url);
        entity.add(new CptAssetsLoading(load));

        load.then(function(pack)
        {
            entity.add(new CptAssetsPackCache(pack));
            entity.removeType(CptAssetsLoading);
        });
    }

    public function update(CptAssetsSrc : CptAssetsSrc){}

    public function updateRemoved(entity : Entity, data : { CptAssetsSrc : CptAssetsSrc })
    {
        if (entity.exists(CptAssetsLoading))
        {
            entity.get(CptAssetsLoading).promise.success;
        }
        if (entity.exists(CptAssetsPackCache))
        {
            entity.get(CptAssetsPackCache).assets.dispose();
        }
    }
}