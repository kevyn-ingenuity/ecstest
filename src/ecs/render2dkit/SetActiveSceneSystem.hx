package ecs.render2dkit;

import edge.*;

import ecs.render2dkit.Components;
import ecs.general.Components;

import kit.scene.Scene;
import kit.scene.Director;

class SetActiveSceneSystem implements ISystem
{
    var directors : edge.View<{ director : CptDirector, view : CptKitView }>;
    var scenes : edge.View<{ scene : CptScene, view : CptKitView, name : CptName }>;
    var entity : Entity;

    public function update (activeScene : CptSetActiveScene) 
    {
        for (director in directors)
        {
            for (scene in scenes)
            {
                if (activeScene.name == scene.data.name.label)
                {
                    var dtr = director.data.view.instance.get(Director);
                    var newScene = scene.data.view.instance.get(Scene);
                    dtr.unwindToScene(newScene, activeScene.transition);

                    entity.removeType(CptSetActiveScene);
                }
            }
        }
    }
}