package ecs.id;

import edge.*;

import ecs.entityIndex.EntityIndex;

//Helper function for CptID indexing for quick searching
class ExtensionIDHelper
{
    static var _index : EntityIndex<Entity, Int> = new EntityIndex<Entity, Int>();

    public static function Get (id : Int) : Array<Entity>
    {
        return _index.Get(id);
    }

    public static function Add (id : Int, entity : Entity)
    {
        _index.Add(id, entity);
    }

    public static function Remove(id : Int)
    {
        _index.Remove(id);
    }

}