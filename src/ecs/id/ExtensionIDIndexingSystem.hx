package ecs.id;

import ecs.id.Components;
import ecs.id.IDGenerator;
import ecs.id.ExtensionIDHelper;

import edge.*;

/**Handles adding and removing of CptID
 *  Also, handles Primary Entity Indexing
*/
class ExtensionIDIndexingSystem implements ISystem
{
    
    public function updateAdded (entity: Entity, data : {id : CptIDExtension})
    {
        ExtensionIDHelper.Add(data.id.number, entity);
    }

    public function update(id : CptIDExtension){}

    public function updateRemoved (entity: Entity, data : {id : CptIDExtension})
    {
        ExtensionIDHelper.Remove(data.id.number);
    }

}