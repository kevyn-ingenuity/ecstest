package ecs.id;

import ecs.id.Components;
import ecs.id.IDGenerator;
import ecs.id.IDHelper;

import edge.*;

/**Handles adding and removing of CptID
 *  Also, handles Primary Entity Indexing
*/
class IDIndexingAndFactorySystem implements ISystem
{
    
    public function updateAdded (entity: Entity, data : {id : CptID})
    {
        data.id.number = IDGenerator.Get();
        IDHelper.Add(data.id.number, entity);
    }

    public function update(id : CptID){}

    public function updateRemoved (entity: Entity, data : {id : CptID})
    {
        IDHelper.Remove(data.id.number);
    }

}