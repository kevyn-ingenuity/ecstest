package ecs.id;

import edge.IComponent;

class CptID implements IComponent
{
    var number : Int;
}

class CptIDExtension implements IComponent
{
    var number : Int;
}

class CptTarget implements IComponent
{
    var id : Int;
}