package ecs.id;

import ecs.id.*;
import ecs.id.Components;

import kit.System;
import kit.scene.Director;

import edge.*;

class IDSystems
{
    public static function Setup (world : World)
    {
        world.render.add(new IDIndexingAndFactorySystem());
        world.render.add(new ExtensionIDIndexingSystem());
    }
}