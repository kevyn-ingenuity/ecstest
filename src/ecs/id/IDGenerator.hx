package ecs.id;

class IDGenerator
{
    static var _currId : Int = 0;

    public static function Get() : Int
    {
        _currId++;
        return _currId;
    }

    public static function Reset()
    {
        _currId = 0;
    }
}