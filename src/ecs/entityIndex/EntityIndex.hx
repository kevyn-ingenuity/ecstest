package ecs.entityIndex;

@:generic
class EntityIndex<TEntity : {}, TKey : Int>
{
    var _index : Map<TKey, Array<TEntity>>;

    public function new ()
    {
        _index = new Map<TKey, Array<TEntity>>();
    }

    public function Add(id : TKey, entity : TEntity) : Bool
    {
        if (_index.exists(id))
        {
            var values = _index.get(id);
            values.push(entity);
        }
        else
        {
            var values = new Array<TEntity>();
            values.push(entity);
            _index.set(id, values);
        }
        return true;
    }

    public function Get(id : TKey) : Array<TEntity>
    {
        return _index.get(id);
    }

    public function Remove(id : TKey) : Bool
    {
        return _index.remove(id);
    }

    public function Clear() : Bool
    {
        for (key in _index.keys())
        {
            _index.remove(key);
        }

        return true;
    }

}